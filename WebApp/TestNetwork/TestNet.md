# The Klingon Testnet

* This document contains the information for the test network that will be used
on the website to make trasaction with our custom token, the Klingon Token (KNT). 

* The Klingon Testnet can be added to Metamask and used on our website with the
following information: 
    * Network Name: Klingon Testnet
    * New RPC URL: https://eth.replit.com
    * Chain ID: 1919250540
    * Currency Symbol: KNT
    
* There is no code that can be pushed related to this the setup of the testnetwork, 
since it was done using the same Moralis server we used for the Metamask connection in
the metamask.js file. Just modifying the currency address and type to be Klingon Token. 
    * ServerURL: https://h9yunbkzk7y4.usemoralis.com:2053/server
    * Sever ID: 7AxJB1yOVIbyRjYibfO1SmOWh0acgGeqnGYXaXJC 
    * Klingon Token address: 7AxJB1yOVIbyRjYibfO1SmOWh0acgGeqnGYXaXJC
