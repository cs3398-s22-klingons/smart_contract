# Step by step guide to acquire Klingon Tokens (KNTs):

* 1: Download a Metamask wallet.
- For android and iOS users, download directly from the corresponding app store. App comes with an integrated browser which allows the use of the wallet on the web.
- For MAC, Windows, or Linux users, download the web browser extension from the official webpage (https://metamask.io/).

* 2: Create an account in Metamask.
- Follow the steps provided by the interface (provide your email, set up password, get your wallet key, etc)

* 3: Add the Binance Smart Chain to Metamask. 
- Open Metamask
- Click on your user icon on the top right
- Click "Settings"
- Click "Networks"
- Click "Add Network" at the bottom
- Write the following information and save: 
    Network Name: BSC Mainnet.
    New RPC URL: https://bsc-dataseed.binance.org/ 
    Chain ID: 56 (0x38 if that does not work)
    Currency Symbol: BNB
    Block Explorer URL: https://bscscan.com/
    
* 4: Switch to the newly added Network and do the following:
- Buy an small amount of BNB (native currency of the network, neccesary to buy KNT and pay gas fees).
- Click on "Assets".
- Click on "Import Token" at the bottom of the list of assets.
- Copy and paste the KNT contract address: 0xBA88059810Dd2b5c96Fe6325F37B465A68c92A2A.
- Click on "Add Custom Token" and now you will be able to see our KNT token in your wallet.

* 5: Buy KNTs. 
- Go to PancakeSwap (a webApp dessigned for Decentralized Finance): https://pancakeswap.finance/swap
- Click on the first asset to swap and select your BNB.
- Click on the second asset to swap.
- Click on "Manage Tokens".
- Click on Tokens and copy and paste the KNT contract address again: 0xBA88059810Dd2b5c96Fe6325F37B465A68c92A2A.
- Now KNT will be available to trade. Choose the amount of BNB you want to spend, approve the transaction, and swap.
- Congratulations! Now you have become an official owner of Klingon Tokens!
