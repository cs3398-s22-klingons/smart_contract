# Creation of liquidity (used to set up the initial price):

Transaction Hash: 0x43d03f502fe4d2e512ba314548d3ae8138cb8f77a9f5e39d4cd7c57f59c51a0e 
Status: Success
Block: 17093821 2694 Block Confirmations
Timestamp: 2 hrs 14 mins ago (Apr-19-2022 10:53:23 PM +UTC)
From: 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 
To: Contract 0x10ed43c718714eb63d5aa57b78b54704e256024e (PancakeSwap: Router v2)  TRANSFER  0.0025 BNB From PancakeSwap: Router v2 To  Binance: WBNB Token
Tokens Transferred: 4
From 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6To PancakeSwap V2: KNT 21 For 10 Klingon Toke... (KNT)
From PancakeSwap: Router v2To PancakeSwap V2: KNT 21 For 0.0025 ($1.05) Wrapped BNB (WBNB)
From Null Address: 0x000...000To Null Address: 0x000...000 For 0.000000000000001 Pancake LPs (Cake-L...)
From Null Address: 0x000...000To 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 For 0.158113883008417966 Pancake LPs (Cake-L...)
Value: 0.0025 BNB ($1.05)
Transaction Fee: 0.017365475 BNB ($7.30)


# Initial transactions between KNT developers:

* Kevin:

Transaction Hash: 0x071a062b5341d7a318155e05c96bf81a2482cd5241f413e0d315200c05a1fae0 
Status: Success
Block: 17089631 6772 Block Confirmations
Timestamp: 5 hrs 38 mins ago (Apr-19-2022 07:23:53 PM +UTC)
From: 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 
Interacted With (To): Contract 0xba88059810dd2b5c96fe6325f37b465a68c92a2a 
Tokens Transferred: From 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6To 0x0b8c3d7e10d7ba7c8f177673f79226090a43f96f For 400 Klingon Toke... (KNT)
Value: 0 BNB ($0.00)
Transaction Fee: 0.000259495 BNB ($0.11)


* Hussain:

Transaction Hash: 0xb578e35e28e5f763eb0b98c2d77e85a67c391d377248d4fc6b8de092169947ea 
Status: Success
Block: 17089479 6782 Block Confirmations
Timestamp: 5 hrs 39 mins ago (Apr-19-2022 07:16:17 PM +UTC)
From: 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 
Interacted With (To): Contract 0xba88059810dd2b5c96fe6325f37b465a68c92a2a 
Tokens Transferred: From 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6To 0x1d6a2c4a79fa0967814eabe1de0159003e697a8c For 400 Klingon Toke... (KNT)
Value:
0 BNB ($0.00)
Transaction Fee:
0.000259435 BNB ($0.11)


* Andres:

Transaction Hash: 0x7370406dc8ef5d1a947977ed50cb5baaf34134d06740f6a5cc50f31927d1c103 
Status: Success
Block: 17069437 27029 Block Confirmations
Timestamp: 22 hrs 34 mins ago (Apr-19-2022 02:31:25 AM +UTC)
From: 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 
Interacted With (To): Contract 0xba88059810dd2b5c96fe6325f37b465a68c92a2a 
Tokens Transferred: From 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6To 0x34555e0397ea7222410e991e15d237951e6d00d1 For 400 Klingon Toke... (KNT)
Value: 0 BNB ($0.00)
Transaction Fee: 0.000259435 BNB ($0.11)
