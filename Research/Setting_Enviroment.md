# Setting local environment for developing and testing smart contracts. 
For developing and testing we need: 
1.	Ethereum based Wallet
2.	Online editor Solidity ( Truffle Suite)  
3.	Source code of Template.

## Setting up developing framework. 
To set up Truffle , requirements are Node.js v8+ LTS and npm ( comes with Node) and Git. 
Verify the installation with command
 node -v

for npm
npm -v

To install truffle we can use command 
npm install -g truffle

To start a project ,select the directory where the project will be located and do command. 
truffle unbox folder-name


You can create a empty project with 
truffle int


# Test Token 

To test  the contract we need to set up a personal block chain. We can use Ganache 
To install we can use the desktop application at      
https://trufflesuite.com/ganache/
or with command 
npm install -g ganache-cli

You can view the github for command line options
 https://github.com/trufflesuite/ganache-cli-archive/blob/master/README.md


Last thing is setting our web3 that will allow to interact with a local or remote Ethereum nodes. To install 
npm install web3


## Setting our Ethereum base wallet 
There are many ways to have your Ethereum wallet. Here we will suggest MetaMask
MetaMask is a browser extension that allows the user to interact with the Ethereum network, with a browser. Bowsers that support this are Firefox, Google Chrome, Brave and Microsoft Edge. Use Chrome webstore or the official page at MetaMask.io.   On the set up page , you create a new wallet. The page will let you set up a backup, using secret phrase. This must documented and place on a secure place, either on paper, local drive , etc.  The phrase will become your wallet seeds.  After completion it will redirect you t the swap service, and this will let you interact with our service and other Ethereum base services and contracts. One finished , using the browser extension , you can buy and swap tokens. This method is user friendly, but there are extra fees (“gas” fee) associate with using this service in comparison to using UniSwap or other methods.  Benefit of using this service is that a backup secret phrase is provided that can recover  a user’s wallet.



Source - Further reading
> [Truffle Suite webpage](https://trufflesuite.com/index.html)
> [Tutorial on setting up enviroment webpage](https://github.com/smartcontractkit/full-blockchain-solidity-course-py)
> [Tutorial on running your own node webpage](https://ethereum.org/en/developers/docs/nodes-and-clients/run-a-node/)