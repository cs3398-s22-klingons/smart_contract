After doing the necessary research, this document will provide the information and necessary links to create our token.

1. Transaction (gas) fees already evaluated. After some test, we determined that the Ethereum mainnet would make the cost of transactions too expensive. Therefore, we will use other network to deploy our token (potentially Binance Smart Chain).

2. Below are attached the necessary links with the information and everything that is required in order for us to create (and sell) our Klingon Tokens (KNT):

https://www.youtube.com/watch?v=K8JoqxOVSaY
https://github.com/openzeppelin/openzeppelin-contracts
https://remix.ethereum.org/

3. To be updated after deciding how and where we want to deploy our token.