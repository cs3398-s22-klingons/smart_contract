# Ethereum 
Ethereum is a blockchain platform to create and run smart contracts, it can execute code so that you can perform any program on Ethereum. 

## Blockchain
A blockchain is a public database that is updated and shared across computers in a network.

"Block" refers to data and state being stored in consecutive groups known as "blocks". If you send ETH to someone else, the transaction data needs to be added to a block to be successful.

"Chain" refers to the fact that each block cryptographically references its parent. In other words, blocks get chained together. The data in a block cannot change without changing all subsequent blocks, which would require the consensus of all parties involved.

# Implemantation

A smart contract is code that lives on the Ethereum blockchain and runs exactly as programmed. Once smart contracts are deployed on the network you can't change them because they are controlled by the logic written into the contract, not an individual.

## Solidity 
   - Solidity is a smart contract programming language on Ethereum. Developed on the top of the EVM, it is similar to the object-oriented programming language that uses class and methods. It allows you to perform arbitrary computations, but it is used to send and receive tokens and store states. Solidity syntax is similar to C++ syntax. Solodity runs in the Ethereum Virtual machine. (EVM)

## Tools for implementation 
   - Truffle( - framework that allows developers to write and test smart contracts.
   - Web3.js - API that interacts with Ethereum through remote procedure calls


Source - Further reading
> [Ethereum webpage](https://ethereum.org/en/developers/docs/)
> [Full Blockchain Free Code Camp github webpage](https://github.com/smartcontractkit/full-blockchain-solidity-course-py)
> [Blockchain easy exercise walkthrough webpage](andersbrownworth.com/blockhain/)