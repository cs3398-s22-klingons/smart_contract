pragma solidity >=0.4.25 <0.9.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Klingon_Token.sol";

contract Test_Klingon_Token {

  Klingon_Token  testToken;
  address owner = msg.sender;
  address spender = address(0x001d3F1ef827552Ae1114027BD3ECF1f086bA0F9);
  address userTest = address(0xc0ffee254729296a45a3885639AC7E10F9d54979);

  // Initialize the Token initial supply as 10000000000 before every test function.
  function beforeEach() public {
  testToken = new Klingon_Token(10000000000);
  
  }
  //Test Token Name
  function testName() public {
  string memory name = "Klingon Token";
  Assert.equal(name,testToken.name(),"Test name");
  }
  //Test Token Symbol
  function testSymbol() public {
  string memory symbol = "KNT";
  Assert.equal(symbol, testToken.symbol(),"Test symbol");
  }
  // Test Decimal value, represents number of decimal use in representation. 18 is standar for ETH tokens.
  function testDecimals() public {
  uint8  decimal = 18;
  Assert.equal(decimal, testToken.decimals(),"Test decimal");
  }
  // Test initial supply 
  function testTotalSupply() public{
  uint256  testSupply = 10000000000;
  Assert.equal(testSupply, testToken.totalSupply(),"Test supply");
  }
     function testBalanceOf() public {
     // Initial Balance of owner should be 0.
  uint256  testBalance = 0;
   Assert.equal(testBalance, testToken.balanceOf(owner),"Test supply");
   }
    
    function testTransfer() public {
   uint256  amount = 100;
   Assert.isTrue(testToken.transfer(spender,amount),"Test transfer");

   }
   // owner and spender dont have any funds.
   function testAllowance() public {
     uint256 allowanceTest = 0;
     Assert.equal(allowanceTest, testToken.allowance(owner,spender),"Test Allowance");
   }

   function testTransferBalance() public {
   uint256  amount = 100;
   testToken.transfer(owner,amount);
   Assert.equal(testToken.balanceOf(owner),amount,"Test transfer Balance");

   }
   function testIncreaseAllowance() public {
   uint256  amount = 100;
    Assert.isTrue(testToken.increaseAllowance(spender,amount),"Test Increase Allowance");

   }
   function testApprove()public {
  uint256  amount = 100;
    Assert.isTrue(testToken.approve(spender,amount),"Test Approve");
   }

   function testDecreaseAllowance() public {
   uint256  amount = 100;
   testToken.increaseAllowance(spender,amount);
   amount = 99;
   Assert.isTrue(testToken.decreaseAllowance(spender,amount),"Test Decrease Allowance");

   }


   function testTransferFrom() public {
   uint256  amount = 10;
   testToken.transfer(spender,amount);
    testToken.approve(spender,amount);
   amount = 0;
   Assert.isTrue(testToken.transferFrom(spender,userTest,amount),"Test TransferFrom");
 }

  
}
