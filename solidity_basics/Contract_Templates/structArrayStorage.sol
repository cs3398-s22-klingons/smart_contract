// SPDX-License-Identifier: MIT
pragma solidity >=0.4.16 <0.9.0;


// 

contract StructStorage {

    uint256 number; // initialized to internal visibility by default

    struct People {             // Make struct
        uint number;
        string name;
    }

    People[] public people;     // initialize dynamic struct array  
    mapping(string => uint256) public nameToNumber; // creates mapping for retrieval purposes

    // Function to add number
    // memory keyword tell the variable to be only in execution

    function addPerson(string memory _name, uint256 _number) public {
        people.push(People({number: _number, name: _name}));        // add the person
        nameToNumber[_name] = _number;                              // match the person to their number
    }
