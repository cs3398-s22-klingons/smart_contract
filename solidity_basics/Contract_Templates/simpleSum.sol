// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;



// Example for if want your contract to accept one kind of external call with two integers:

contract Simple {
    uint sum;
    function taker(uint _a, uint _b) public {
        sum = _a + _b;
    }
}