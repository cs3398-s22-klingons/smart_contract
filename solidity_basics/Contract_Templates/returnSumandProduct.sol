// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;


// Example for if you want to to pass in two arguments and return sum and product

// Functions can be declared pure in which case they promise not to read from or modify the state.
contract Simple { 
    function arithmetic(uint _a, uint _b)
        public
        pure            
        returns (uint o_sum, uint o_product)
    { 
        o_sum = _a + _b;
        o_product = _a * _b;
    }
}

// Version 2 below explicitly states return values

contract Simplev2 {
    function arithmetic(uint _a, uint _b)
        public
        pure
        returns (uint o_sum, uint o_product)
    {
        return (_a + _b, _a * _b);
    }