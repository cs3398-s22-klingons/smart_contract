// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;


// 

contract StructStorage {

    uint256 number; // initialized to internal visibility by default

    struct People {             // Make struct
        uint number;
        string name;
    }

    People[] public people;     // initialize dynamic struct array  



    function addPerson(string memory _name, uint256 _number) public {
        people.push(People({number: _number, name: _name}));
    }




    function store (uint256 _input) public {
        number = _input
    }

    // view and pure keywords do not require blockchains transactions 
    function getter () public view returns(uint256) {
        return number;
    }
}