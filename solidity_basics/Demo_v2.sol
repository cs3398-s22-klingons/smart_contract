pragma solidity ^0.6.12;

// New provisional demo for Sprint #2. This program was created for educational purposes only.

// In this program, we are simulating the adquisition of an NFT (Non Fungible Token). 
// NFTs are an special type of token which is unique, which means that each NFT is different and can only have 1 owner.
// With this code, we are simulating the creation of an NFT and assigning a value to it (which can be modified.
// After deploying the NFT to the blockchain, a wallet owner can buy the NFT by paying the necessary price.
// Once the NFT is owned by 1 person, it becomes unavailable, so the transaction will display the corresponding error message

// This demo is intended to explain how transactions in the blockchain work with a practical example before we show an actual
// transaction with our own cryptocurrency using Metamask wallets in the final sprint.

contract Demo_v2 {
    enum Statuses { Available, Owned }
    Statuses currentStatus;

    event Own_NFT (address _buyer, uint _value);

    address payable public owner;

    constructor() public {
        owner = payable(msg.sender);
        currentStatus = Statuses.Available;
    }

    modifier onlyWhileAvailable {
        require(currentStatus == Statuses.Available, "This NFT is currently owned.");
        _;
    }

    modifier NFT_cost (uint _amount) {
        require(msg.value >= _amount, "ETH is not enough");
        _;
    }

    receive () external payable onlyWhileAvailable NFT_cost (1 ether) {
        currentStatus = Statuses.Owned;
        owner.transfer (msg.value);
        emit Own_NFT (msg.sender, msg.value);
    }
}