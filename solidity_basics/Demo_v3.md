# Optimizations and examples.

* This demo won't be a solidity program anymore. 
* Instead, we will be showing examples of our own transactions to show why we decided to migrtate our token from the Ethereum Network to the Binance Smart Chain.

# Background.

1. Ethereum was revolutionary because it was the first cryptocurrency which allowed users to build proyects on it.
* Instead of being just a token for making transactions, it was created as a network which contains the Ethereum Virtual Machine.
* The Ethereum Virtual Machine (EVM) was the origin of the solidity language, which we can use to implement smart contracts as shown in the previous sprints.

2. Ethereum was created with a Proof of Work mechanism, meanings that new tokens had to be mined in order to increase the supply.
* However, as the demand for Ethereum increased, the Proof of Work mechanism became inefficient, since Ethereum started to present scalability issues.
* These issues led to a huge increase in transaction fees, to the point in which today an avarage transaction costs about $30.


# Our Solution: Binance Smart Chain.

* In the present, Ethereum scalability issues are solved by what is known as Proof of Stake mechanism.
* Proof of Work (POW) relies on miners to work consistently, requiring very powerful hardware and energy consumption.
* Proof of Stake (POS) relies on staking; every person who owns the asset can stake it; receiving a percentage of the fees that are consumed when transacting and minting.
* The last mechanism is significantly more energy efficient, and is used by what is known as Layer-1 solutions; networks with the capabilities of ETH but using POS.
* The Binance Smart Chain is currently the major Layer-1, and the one we decided to use to deploy our KNT.

# Links to personal transactions used to demonstrate optimizations:

1. Ethereum Network:
* https://etherscan.io/tx/0x0a91952d6ebf88fa79589c4e9a3d601843dee93964a96a0732020b4efce5d383

2. Binance Smart Chain Network:
* https://www.bscscan.com/tx/0x546b5a8ac53005a9190bf55278b5b8456064c426bc8ad1925072630033c7cfe1




