pragma solidity ^0.6.12;

//This demo program will implement a simple token counter using the programming language Solidity.
//After writing the code, it will be necessary to compile it and deploy in order to run it.
//When deployed, we can use (call) the functions and check or modify the values stored in them.

// Code has been optmized since sprint #1, deleting the amount of functions required.
// A different demo will be used for sprint #2 (to be uploaded soon...)

contract Demo {
    uint public KNT = 0;


//This function increases KNT amount by 1.
    function buyOneKNT() public {
        KNT = KNT ++;
    } 

//This function reduces KNT amount by 1.

    function sellOneKNT() public {
        KNT = KNT --;
    } 
}