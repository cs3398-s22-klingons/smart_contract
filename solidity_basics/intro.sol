// SPDX-License-Identifier: GPL-3.0
// Basics of Solidity
// Shows basic syntax and structure of language for Solidity
// 
// Documentation for Solidity Language is found in the next line.
// https://docs.soliditylang.org/en/v0.8.12/introduction-to-smart-contracts.html
// REMIX Ethereum IDE is available in browser with the Code examples from the above documentation
// REMIX will allow you to understand and the concepts of the contract/ethereum interaction

// ***TUTORIAL START***
// Line 1 tells you that the source code is licensed under the GPL version 3.0. 
// Machine-readable license specifiers are important in a setting where publishing the source code is the default.


// Version Number - This ensures the contract is not compiled with a new comiler version that may break the code
// This pragma instruction instructs anything from version 0.6.0 up to but not including 0.9.0
pragma solidity >=0.6.0 <0.9.0;


// Let's look a contract and some of the common types. Contract is a key word in this SOLIDITY.

contract TypeExamples {

    uint256 integerNumber = 5;              // uint256 is used for integers as required by some Smart Contract Protocols 
    bool booleanValue = true;               // boolean values
    string stringValue = "Hello World";     // string values
    address addressValue;                   // addresses are used to link ethereum accounts 
    bytes32 byteValue = "cat";              // values converted to bytes. Can be set bytes1 - bytes32
}




// ***Visibility and Getters***

// Solidity requires state variables and functions to have a visibility setting. This allows different interactions to be possible depending on the setting.

// State Variable Visibility
//  1. Public variable-     differ from internal ones only in that the compiler automatically generates getter functions for them which allows other contracts to read their values. 
//  2. Internal variable-   Can only be accessed from within the contract they are defined in and in derived contracts.
//  3. Private variable-    Like internal ones but they are not visible in derived contracts.

// Function Visibility
//  1. External functions   - part of the contract interface, which means they can be called from other contracts and via transactions
//  2. Public functions     - part of the contract interface and can be either called internally or via message calls.
//  3. Internal functions   - can only be accessed from within the current contract or contracts deriving from it. They cannot be accessed externally.
//  4. Private functions    - like internal ones but they are not visible in derived contracts.





// ***Simple Types of Contracts***

// 1. State variables - variables whose values are permanently stored in contract storage.
contract SimpleState {
    uint storedData; // State variable
    // ...
}

// 2. Functions - executable units of code. Functions are usually defined inside a contract, but they can also be defined outside of contracts.
contract SimpleAuction {
    function bid() public payable { // Function
        // ...
    }
}
// Helper function defined outside of a contract
function helper(uint x) pure returns (uint) {
    return x * 2;
}

// 3. Function modifiers - amend the semantics of functions in a declarative way (see Function Modifiers in the contracts section).
contract Purchase {
    address public seller;

    modifier onlySeller() { // Modifier
        require(
            msg.sender == seller,
            "Only seller can call this."
        );
        _;
    }

    function abort() public view onlySeller { // Modifier usage
        // ...
    }
}

// 4. Events - convenience interfaces with the EVM logging facilities.
contract SimpleAuctionEvent {
    event HighestBidIncreased(address bidder, uint amount); // Event

    function bid() public payable {
        // ...
        emit HighestBidIncreased(msg.sender, msg.value); // Triggering event
    }
}

// 5. Errors - allow you to define descriptive names and data for failure situations.
/// Example -- Not enough funds for transfer. Requested `requested`, but only `available` available.
error NotEnoughFunds(uint requested, uint available);

contract Token {
    mapping(address => uint) balances;
    function transfer(address to, uint amount) public {
        uint balance = balances[msg.sender];
        if (balance < amount)
            revert NotEnoughFunds(amount, balance);
        balances[msg.sender] -= amount;
        balances[to] += amount;
        // ...
    }
}

// 6. Structs - custom defined types that can group several variables
contract Ballot {
    struct Voter { // Struct
        uint weight;
        bool voted;
        address delegate;
        uint vote;
    }
}

// 7. Enums - used to create custom types with a finite set of ‘constant values’
contract PurchaseEnum {
    enum State { Created, Locked, Inactive } // Enum
}




// Example of a Token Contract from https://docs.soliditylang.org/en/v0.8.12/contracts.html#creating-contracts
// Walkthrough with comments

contract OwnedToken {
    // `TokenCreator` is a contract type that is defined below.
    // It is fine to reference it as long as it is not used
    // to create a new contract.
    TokenCreator creator;
    address owner;
    bytes32 name;

    // This is the constructor which registers the
    // creator and the assigned name.
    constructor(bytes32 _name) {
        // State variables are accessed via their name
        // and not via e.g. `this.owner`. Functions can
        // be accessed directly or through `this.f`,
        // but the latter provides an external view
        // to the function. Especially in the constructor,
        // you should not access functions externally,
        // because the function does not exist yet.
        // See the next section for details.
        owner = msg.sender;

        // We perform an explicit type conversion from `address`
        // to `TokenCreator` and assume that the type of
        // the calling contract is `TokenCreator`, there is
        // no real way to verify that.
        // This does not create a new contract.
        creator = TokenCreator(msg.sender);
        name = _name;
    }

    function changeName(bytes32 newName) public {
        // Only the creator can alter the name.
        // We compare the contract based on its
        // address which can be retrieved by
        // explicit conversion to address.
        if (msg.sender == address(creator))
            name = newName;
    }

    function transfer(address newOwner) public {
        // Only the current owner can transfer the token.
        if (msg.sender != owner) return;

        // We ask the creator contract if the transfer
        // should proceed by using a function of the
        // `TokenCreator` contract defined below. If
        // the call fails (e.g. due to out-of-gas),
        // the execution also fails here.
        if (creator.isTokenTransferOK(owner, newOwner))
            owner = newOwner;
    }
}


contract TokenCreator {
    function createToken(bytes32 name)
        public
        returns (OwnedToken tokenAddress)
    {
        // Create a new `Token` contract and return its address.
        // From the JavaScript side, the return type
        // of this function is `address`, as this is
        // the closest type available in the ABI.
        return new OwnedToken(name);
    }

    function changeName(OwnedToken tokenAddress, bytes32 name) public {
        // Again, the external type of `tokenAddress` is
        // simply `address`.
        tokenAddress.changeName(name);
    }

    // Perform checks to determine if transferring a token to the
    // `OwnedToken` contract should proceed
    function isTokenTransferOK(address currentOwner, address newOwner)
        public
        pure
        returns (bool ok)
    {
        // Check an arbitrary condition to see if transfer should proceed
        return keccak256(abi.encodePacked(currentOwner, newOwner))[0] == 0x7f;
    }
}

// Please reference https://docs.soliditylang.org/en/v0.8.12/introduction-to-smart-contracts.html

