pragma solidity ^0.4.25;

contract RealEstate {
    address owner;
    address seller;
    address buyer = 0x0;
    string streetAddress;
    string status;
    uint256 price;
    
    //This function creates a listing for a house
    function RealEstate () {
        //current owner
        seller = msg.sender;
        //house address
        streetAddress = "123 Main ST, San Marcos TX, 78666";
        //house price
        price = 1000000; // KNT
        status = "FOR SALE";
    }

    //Function to collect buyer's info and set value to send
    function getBuyer() {
        buyer = msg.sender;
        msg.value == price;
    }

    function getAddress() view public returns (string) {
        return streetAddress;
    }

    function getPrice() view public returns (uint256) {
        return price;
    }

    function getStatus()view public returns (string){
        return status;
    }

    function getSeller() view public returns (address) {
        return seller;
    }

    function getOwner()view public returns (address) {
        return owner;
    }

    function returnBuyer()view public returns (address) {
        return buyer;
    }
    
    //transfer KNT to owner and change owner
    function buyHouse () public{
        owner.send(msg.value);
        owner = buyer;
        status = "SOLD";
    }

    //Function to reset buyer's and seller's info
    function reset() public {
        buyer = 0x0;
        owner = msg.sender;
        seller = msg.sender;
        streetAddress = "123 Main ST, San Marcos TX, 78666";
        price = 1000000; // KNT
        status = "FOR SALE";
    }
}