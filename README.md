# Smart Contracts
> Group members: Kevin Garcia Lopez, Andres Correa-Martinez, Jason McKinnerney, Pedro Osorio Lopez, Husain Alshaikhahmed.
We are creating a blockchain app to create smart contracts. 
We are creating this app mainly for the financial, renting, and banking sectors. But can also be used by the general public for contracts.
We hope to make it easier and safe for people to make contracts and exchange digital goods. 



## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Sprint 1](#sprint-1)
* [Sprint 2](#sprint-2)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
Image Logo Idea/ Rough Draft
<img src = "https://bitbucket.org/cs3398-s22-klingons/smart_contract/raw/81802cddaa9ab95ded130491085118421a6c0695/Research/KlingonTokenLogo.png"><br>
    - The use of blockchain and smart contracts allow for the automation of execution of contracts which allows
    - it's users to be immediately certain of the outcome of the transaction. However, as this technology is still
    - growing, many people are unaware/unable to use this means due to unfamiliarity. Our intention is to create a
    - product that will be user friendly to expand the user base for this technology.



## Technologies Used
- C++ 
- Solidity
- HTML
- Jira
- REMIX IDE
- Bitbucket
- CircleCI
- Ethereum


## Features
List the ready features here:

- AES Encryption: Encryption done in 256 bit keys to make sure the user's data is safe.
    * Story: I, Kevin, as a virtual wallet user, I want the application to be safe for transactions, so that I can feel comfortable and safe when using it.
- Standard GUI: Basic user interface to facilite transactions and management of the smart contracts.
    * Story: I, Kevin, as an application user, I want an application to be user friendly so that I know what I am doing and can control all aspects.
- Tutorial and Background: To provide information to the user about our services and how they are improving.
	* Story: I, Pedro, as a student of Texas State University would like to search for more information about what exactly are smart contracts and their evolution since they originated.

## Screenshots

<!-- If you have screenshots you'd like to share, include them here. -->


## Setup
What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

There is no need to install further dependencies (up to this point). Remix, the Ethereum IDE, is an online tool that allow us to write, build and run the neccesary code to implement smart contracts.


## Usage
How does one go about using it?

In order to implement smart contracts, we will be using the language solidity, the language developed by the Ethereum blockchain. 
One of the best things of using this programming language is the fact that no additional hardware is required in order to be used, since our project can be coded in Remix, Ethereum IDE.

Please refer to solidity_basics directory for a quick tutorial and additional documentation


## Project Status

Project is completed for class purposes. Sprints 1, 2 and 3 are complete. 

## Sprint 1

- Kevin: Mainly focused on researching and learning technologies that we can use in out developement, such as encryptions. Also design an initial
    plan for our user interface. 
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/EncryptionResearch.md
        - Types of encryptions avaliable in Solidity. 
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/Ethereum.md
        - Ethereum research, how smart contracts work on ethereum.  
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/SMCONDesign.png
        - Plan for our interface.
        
- Jason: Focus on research of Solidity and basic template builds to expand contract and currency flexibility. Built solidity basic introduction for team.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/solidity_basics/Contract_Templates/
       - Contract templates built for familiarization and experiment
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/solidity_basics/intro.sol
       - Introduction to solidity, a custom walkthrough to show basics of Solidity Contracts.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/FullSolidityTutorial_link.md
       - Solidity tutorial through video for quick introduction
       
- Pedro: Focus on research and starting our first steps in the creation of our own token and smart contracts. Added demo, our first fully functional
    solidity program to simulate a transaction in the blockchain, and started a template which includes several functions to set up the full token implementation
   * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/solidity_basics/Demo.sol.
       - Demo program used in our class presentation.
   * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/solidity_basics/Our_Token/OurToken_FirstSteps.sol
       - Template program starting the creation of our cryptocurrency that will be updated in the future.

- Andres: Focus on research of Solidity and setting up enviroment for deployment of contract , without the use of Remix IDE. Research of Blockchain conept. Researched on the tools used for developingsmart contracts.
	*https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/Setting_Enviroment.md
	-Documentation of implementing enviroment without te use of Remix IDE.


### Room for Improvement
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

- What went well:
    *  We were able to work together and sync the repository successfully.
    *  Kevin – Was able to complete my tasks and get myself familiar with Solidity.
    *  Pedro - Adquired plenty of new knowledge about the blockchain and Solidity, and succesfully started experimenting with Remix to compile and deploy code.
	*  Andres- we all have good knowledge of Solidity and how to implement and deploy smart contracts.

- Room for improvement:
    * Improvement to be done 1: Finish a fully functional token to operate using smart contracts.
    * Improvement to be done 2: Apply SOLID principles and/or other material learned in this class to further improve our code.
    * Improvement to be done 3: Continue to improve on Solidity fundamentals to expand contract flexibility and security.
    * Meet more regularly to discuss our progress.
    * Make sure all the tasks on our Jira board are assigned and completed on time.
    
- What is impeding us from doing better?
    * Lack of subject knowledge. Continuous improvement and familiarity with the concept will allow better understanding of individual steps to reach goal.
    * Not being too familiar with the technologies we are using for our project. 

- What can I do better?
    * Jason - Strive to become subject matter expert with Solidity. This will allow faster changes using team ideas to be implemented and expanded on. This will be apparent through dialouge among team and production of clear code.
    * Pedro - Acquire more knowledge in order to continuously improve our code. Expanding the uses and optimizing our technology will be key in order produce a safe token to use.
    * Kevin – Communicate more with my teammates by giving progress reports and asking for clarification/feedback when needed. We will know I am doing this by always being up to date on what each other is working on and what is missing or needs attention. 
	* Andres- was not able to produce what I wanted due to personal reasons. Need to be on time with my task and discuss what I can implement before it is already implemented.Produce code for the second sprint. 
- Next Steps for Sprint 2:
    * Feature to be added 1: Allow others to use our token and test some transactions.
    * Feature to be added 2: Add optimizations (reduce transaction fees, improve scalability, etc).
    * Create a working user interface - Kevin
    * Start working on main template - Kevin
    * Continue to develop and organize contract templates that focus on single responsibilities. - Jason
    * Expand the utility of the token so that full transactions can be simulated.
    * Start the transition from a simple simulated token to a fully functional one that can be stored in a crypto wallet.
    
    
## Sprint 2

- Kevin: Focused on creating and adding functionalities to the website to interact with smart contracts and virtual wallet. 
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/0885717977f3f1e54a67a887d436ece3abb86b7d
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/pull-requests/1
        - Added Metamask functionality to our website. 
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/83fccf8ec8f5368445674de8ee7bea7b40976b30
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/49279e091520f1ec928d25407cd6243be16f2896
        - Created logo and added it to website.  
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/0fdb8ac8e615340f9f48c45cd63ea2a1af038cc2
        - Connected website to a deployed smart contract
        
- Pedro: Focused on developing the code and creating our Klingon Token.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/solidity_basics/Demo_v2.sol
        - Demo_v2.sol created, solidity program made from scratch in which we simulate an actual transaction (which can only be performed once).
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Our_Token/Klingon_Token.sol
        - Klingon_Token.sol created, file which implements the ERC20 interface and contains all the variables that we needed to deploy our token to the network.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/TokenCreation.md
        - TokenCreation.md created, which provides an explanation about why we decided to release the token in the Binance Smart Chain and some other useful information.
        
- Husain: Focused on testing parts of the code and implanting error handling functions such as required ().
     * https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/f35966b456c5104f4e94de6a593dd5569d43431f?at=error-handling-test-demo-helped with the team research and presentation
       - research and presentation
      *  https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/d69762ad188b38fbbe3e461d9f7e2b94da5ad186
      - Review code and implement the SOLID principles
        
        Andres : Focuse on the layout of the website and fixing interactions with the smart contract. 
		https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/WebApp/
        
- Jason: Focused on learning correct format and requirements to match coin to Ethereum Standards ().
     * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Research/EthereumStandards.txt
        - Research, links to numerous standard practices that we were unaware of. Major reformatting of the currency will be required.
    
        
### Restrospective / Room for Improvement / Next Steps
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

- What went well:
    *  We were able to sync the repository and use branches successfully.
    *  Created a website that successfully connects to the virtual wallet and smart contract. 
    *  Kevin – Added the main functionalities needed for our website to interact with smart contracts on the blockchain.
    
    *  Pedro - Succesfully created the Klingon Token, deploying 10000 coins.
    *  Significantly improved my knowledge on solidity.
    *  Was able to write/understand the code for a transaction.
    
    * Husain was able to reviw soldity code and implemnt SOLID princlables 
    * Review code and implement the SOLID principles
- Room for improvement:
    *  Implement test cases for smart contract.
    *  Get more used to working with Gitkraken.
    *  Become more famlier with Bitbuckket.
    
    
    
- What is impeding us from doing better?
    * Something that might be impeding us from doing better is not having enough progress reports/communication among teammates.
    * Not every person in the team is currently doing their part.
    * Dedicate more time for meetings outside of the class

- What can I do better?
    * Kevin - Gain better understanding on how to operate our own blockchain network and how we can use our own token. 
       We will know I am doing this by seeing that our website is using the Klingon token when interacting with smart contracts.
       
    * Pedro - Overall work more with my team and be able to provide more help to them so that they can understand this proyect (and how crypto works in general) better.
    * Jason - Spend more time focusing on tasks.
    * Husain - Offer help to other team members even if they did not ask.
    * Andres - Needed help and did not ask, could not get my work completed. 
	
- Next Steps for Sprint 3:
    * Kevin - Connect our final contract to website.
    * Kevin - Add our custom token to virtual wallet to make transactions on our website.
    * Pedro - Allow every user with a metamask wallet to purchase our token.
    * Pedro - Further optimize and add utility to the token.
    * Pedro - Add liquidity and set up the initial price for each KNT (Klingon Token).
    * Husain - Implement marketing strategy.
    * Husain - Implement more testing on Solidty code. 
	* Andres - Continue on the website work and making test. 
    * Jason - Rework format of currency to match standards
    * Jason - Become expert on current standards of practices in Ethereum.
    
## Sprint 3
        
- Pedro: Focused on giving value to the KNTs, optimize the token, make transactions and write a tutorial for new users to acquire Klingon Tokens.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Our_Token/BuyKNT_Tutorial.md
        - BuyKNT_Tutorial.md created, which serves as a guide for new users to succesfully set up a Metamask wallet and use it to acquire some of our KNTs.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Our_Token/TransactionRecord.md
        - TransactionRecord.md created, file which contains the transactions that were neccesary to set up the token and sharing it with my teammates.
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/solidity_basics/Demo_v3.md
        - Demo_v3.md created for presentation purposes, which reports our reasons to use the Binance Smart Chain network and explains the differences between Proof of Work and Proof of Stake.
- Husain: Focused on improving user experience, set up Metmaske wallet to receive the token.
     *  https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/0c22cd9162fda6df27d242397f62aca85f4b65c5
     - Metamask wallet set up
     *  https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/6a93f752d56bf22d24709bda8d454a0bd9cb31bf  
     -  improving user experience
     - Add bootstrap
     - No pull request for this branch, it is included in the styling branch. please note this was done intentionally as I discussed it with my teamamtes.Please do not take points off.
    
- Andres: Focuse on testing the contract and creating a report that consisted of the process of creating and developing the token.
	* https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Truffle/
		- Jira branch, SMAR-31:Truffle test for main contract.
	*https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/master/Report/
		-Jira branch, SMAR-45: Token report.
- Kevin: Focused on creating a smart contract that could use our token to make transanctions and creating a test network, and implementing them into our website. 
    * https://cs3398s22klingons.atlassian.net/browse/SMAR-40 
        - Set up the Klingon test network to make transactions with the Klingon Token. 
        - https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/ded577442d82d46c984fae4ba54ba9d2a56b04dd 
        - There is no pull request on this task because I had trouble when pushing to remote branch. The commits went directly into master not into the branch so a pull request was not possible. Dr. Lehr Approved. 
    * https://cs3398s22klingons.atlassian.net/browse/SMAR-44
        - Created a final smart contract that uses klingon tokens to simulate buying a house. 
        - https://bitbucket.org/cs3398-s22-klingons/smart_contract/commits/d296c6d9b6361d9532a5bac4adc83b008ce8058c
    * https://cs3398s22klingons.atlassian.net/browse/SMAR-39
        - Incorporated the final contract into the website, where users can interact with it using the Klingon test network, metamask, and Klingon tokens. 
        - https://bitbucket.org/cs3398-s22-klingons/%7Bbc63218a-2a2e-4cf0-8704-34f795157391%7D/pull-requests/10
- Jason: Focused on website security certification and Token Standardization
    * https://bitbucket.org/cs3398-s22-klingons/smart_contract/src/32a4d197be1a/Research/FutureAdditions/Website%20Security/googleSecurity.html?at=master 
        - Research and Future Development planning for external Website Certification 
        
### Restrospective / Room for Improvement / Next Steps
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

- What went well: 
    - Team: 
        * We were able to finish our functional token and deployed it into the Binance Smart Chain for public use.
        * Sucessfully created a website that uses Klingon tokens to make transactions. 
        * We worked well togother and were able to assemble all the components into the main and final project. 
        * We were able to demonstrate how our token can be traded and used for transactions. 
    
    -  Pedro 
        *  Set up initial liquidity and price for the Klingon Token.
        *  Added a full tutorial for new users to acquire our tokens.
        *  Shared a fraction (20%) of the token supply with my teammates.
        *  Succesfully optimized the token to make transactions cheap.
        *  Performed actual transactions in front of the class for the final presentation.
    - Husain 
        * Succesfully finshed the project on time.
        * Learned alot about team work.
        * Expanded my knowledge about Solidity programming
	- Andres 
		* Succesfully finish  3 task. 
		* Testing the contract help me learn more about the technology of Etherium and Smart Contracts.
    - Kevin
        * Created a functional smart contract that sucessfully uses Klingon tokens. 
        * Created a Klingon test network that can be added to the Metamask wallet tbe used on our website.
    - Jason
        * Improved skill to customize simple smart contracts for ease of use and easy-user driven implementation. 
        * Website certification understanding improvement for implementation if decision to launch demo into commercial platform. 
		
    
- Room for improvement:
    *  Provide more liquidity to the token so that transactions don't affect its price too much.
    *  Improve user experience.
    
    
- What is impeding us from doing better?
    * Smart contracts are still a new technology which most people still don't fully understand. If these contracts became more accesible to society, we could further extend the scope of the project.
    * Fully understand the blockchain technology; however, I do believe we are in the right track.
	* Testing this technology can be challenging , due to the many components and methods  of the blockchain,
	  example working with mock addresses and token to test transaction

- What can I do better?   
    * Pedro - Add more functionality to our token and learn more about git technology to improve my skills working as a team in the future for a company.
    * Husain - Learn more about Solidity programming and testing with truffle.
	* Andres - Failed to implement test and everything I wanted for the website due to complexities of web3. 
    * Kevin - Add more functionalities to the website to make the interactions with smart contracts more smooth. We will know that I am doing this by seeing that the website does not require a reload after each transaction.
    * Jason - Communication lacked in final sprint due to close family death. Failed to communicate in professional manner.
    
	
- Next Steps for the future:
    * Create a proyect, such as a game or a shop, in which our tokens can be used to improve the characters, adquire items, etc.
    * Put  the token in large platforms such as Coinbase. 
    * Kevin - create a more interactive website that shows the changes as soon as the smart contract finishes a transactipn without having to reload the page.
    * Kevin - Add a functionality to the website that would allow the use to make real trasanctions using the Binance Smart chain.
	* Andres - Add test to website page, and create a terminal like window on the site that can display info on all functionality of site.
    * Jason - Token needs to be standardized for different platforms to deploy coins.

## Acknowledgements
Give credit here (to be updated).
- This project was inspired by...
- This project was based on [Solidity, Blockchain, and Smart Contract Course_freecodecamp](https://youtu.be/M576WGiDBdQ).
- Many thanks to...


## Contact
Created by [@flynerdpl](https://www.flynerd.pl/) - feel free to contact me!


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->
