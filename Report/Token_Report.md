# Development 

* Created using solidity  

* Developed using Remix IDE  

* Tested using Truffle suite  

* Deployed in Binance , Smart Chain  


# Token info  
* Name “Klingons ” 

* Symbol “KNT ” 

* Total Supply 10,000 tokens. 

* Initial price ~$ 1.00 

* Interface used:  

* ERC-20 - A standard interface for fungible (interchangeable) tokens, like voting tokens, staking tokens or virtual currencies. 


# Logo 

<img src = "https://bitbucket.org/cs3398-s22-klingons/smart_contract/raw/81802cddaa9ab95ded130491085118421a6c0695/Research/KlingonTokenLogo.png"><br>


# Distribution    
* 80% for sale  

* 20 % for developer  

* Average gas fee 0.05 - 0.10cents  

* Creation of liquidity (used to set up the initial price): $1:00

* Transaction Hash:
0x43d03f502fe4d2e512ba314548d3ae8138cb8f77a9f5e39d4cd7c57f59c51a0e Status:
Success Block: 17093821 2694 Block Confirmations Timestamp: 2 hrs 14 mins ago (Apr-19-2022 10:53:23
PM +UTC) From: 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 To: Contract
0x10ed43c718714eb63d5aa57b78b54704e256024e (PancakeSwap: Router v2) TRANSFER 0.0025 BNB
From PancakeSwap: Router v2 To Binance: WBNB Token Tokens Transferred: 4 From 
0x720b879579a1fb8539b8679d54e7fa2ce52c34a6To PancakeSwap V2: KNT 21 For 10 Klingon Toke...
(KNT) From PancakeSwap: Router v2To PancakeSwap V2: KNT 21 For 0.0025 ($1.05) Wrapped BNB
(WBNB) From Null Address: 0x000...000To Null Address: 0x000...000 For 0.000000000000001 Pancake
LPs (Cake-L...) From Null Address: 0x000...000To 0x720b879579a1fb8539b8679d54e7fa2ce52c34a6 For
0.158113883008417966 Pancake LPs (Cake-L...) Value: 0.0025 BNB ($1.05) Transaction Fee: 
0.017365475 BNB ($7.30).
Transaction History as 4/25/2022, 9:00pm 

* Initial transaction between developers  
See : https://bitbucket.org/cs3398-s22-klingons/smart_contract/raw/830f9558e544bd6fe6caae296ddb3c9fa460b16e/Our_Token/TransactionRecord.md


# Transaction History as 4/25/2022, 9:00pm 
<img src = "https://bitbucket.org/cs3398-s22-klingons/smart_contract/raw/b0c94a5a856ab6ca6da0fa59a0ee4152ef4f890d/Report/token_Transanction.PNG"><br>


# Testing  

* Development 
<img src = "https://bitbucket.org/cs3398-s22-klingons/smart_contract/raw/b0c94a5a856ab6ca6da0fa59a0ee4152ef4f890d/Report/Test_Development.PNG"><br>

* Test output
<img src = "https://bitbucket.org/cs3398-s22-klingons/smart_contract/raw/b0c94a5a856ab6ca6da0fa59a0ee4152ef4f890d/Report/test_result_.PNG"><br>


# Purchase Guide  

1: Download a Metamask wallet. 

2: Add the Binance Smart Chain to Metamask. 

3: Switch to the newly added Network and do the following 

4:Import Token 

5: "Manage Tokens". 

6: Do Trade! Now you have become an official owner of Klingon Tokens! 

For more info see step by step guide on smart_contract /Our_Token/BuyKNT_Tutorial.md 




